((nil
  (bug-reference-bug-regexp . "#\\(?2:[[:digit:]]+\\)")
  (bug-reference-url-format . "https://gitlab.com/leungbk/helm-gdq-schedule/issues/%s")
  (sentence-end-double-space . nil))
 (emacs-lisp-mode
  (indent-tabs-mode . nil)
  (outline-regexp . ";;[;*]+[\s\t]+")))
