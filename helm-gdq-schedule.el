;;; helm-gdq-schedule.el ---  View the GDQ schedule -*- lexical-binding: t -*-

;; Author: Brian Leung <leungbk@mailfence.com>
;; URL: https://gitlab.com/leungbk/helm-gdq-schedule
;; Version: 0.1
;; Package-Requires: ((emacs "26.1") (helm "3.3") (helm-org) (ts "0.1"))
;; Keywords: schedule

;;; Commentary:

;; This package extends `helm' to view the GDQ schedule. `org-capture'
;; can be used to mark speedruns of interest and save them to an
;; agenda.

;;; License:

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see
;; <http://www.gnu.org/licenses/>.

;;; Code:

(require 'cl-lib)
(require 'dom)
(require 'helm)
(require 'helm-net)
(require 'helm-org)
(require 'parse-time)
(require 'ts)

;;* Customization
(defgroup helm-gdq-schedule nil
  "View the GDQ schedule."
  :group 'helm)

(defcustom helm-gdq-schedule-start-timestamp-format-string "%a %b %d %H:%M"
  "Format string for a candidate run's start timestamp."
  :type 'string
  :group 'helm-gdq-schedule)

(defcustom helm-gdq-schedule-end-timestamp-format-string "%H:%M"
  "Format string for a candidate run's end timestamp."
  :type 'string
  :group 'helm-gdq-schedule)

(defcustom helm-gdq-schedule-run-format-string "[%s - %e]: %g — %r\n%d"
  "Format string for a candidate run."
  :type 'string
  :group 'helm-gdq-schedule)

(defcustom helm-gdq-schedule-cache-time 5
  "Time in minutes after which subsequent calls to `helm-gdq-schedule' should make
 new network calls."
  :type 'float
  :group 'helm-gdq-schedule)

(defcustom helm-gdq-schedule-org-capture-templates
  '(("g" "Speedrun" entry (file "~/notes.org")
     "* [${game}] %?${run}\nSCHEDULED: ${start}\nhttps://twitch.tv/gamesdonequick\n"))
  "`org-capture' templates for `helm-gdq-schedule'."
  :type
  ;; copied from `org-capture-templates'
  (let ((file-variants '(choice :tag "Filename       "
                                (file :tag "Literal")
                                (function :tag "Function")
                                (variable :tag "Variable")
                                (sexp :tag "Form"))))
    `(repeat
      (choice :value ("" "" entry (file "~/org/notes.org") "")
              (list :tag "Multikey description"
                    (string :tag "Keys       ")
                    (string :tag "Description"))
              (list :tag "Template entry"
                    (string :tag "Keys           ")
                    (string :tag "Description    ")
                    (choice :tag "Capture Type   " :value entry
                            (const :tag "Org entry" entry)
                            (const :tag "Plain list item" item)
                            (const :tag "Checkbox item" checkitem)
                            (const :tag "Plain text" plain)
                            (const :tag "Table line" table-line))
                    (choice :tag "Target location"
                            (list :tag "File"
                                  (const :format "" file)
                                  ,file-variants)
                            (list :tag "ID"
                                  (const :format "" id)
                                  (string :tag "  ID"))
                            (list :tag "File & Headline"
                                  (const :format "" file+headline)
                                  ,file-variants
                                  (string :tag "  Headline"))
                            (list :tag "File & Outline path"
                                  (const :format "" file+olp)
                                  ,file-variants
                                  (repeat :tag "Outline path" :inline t
                                          (string :tag "Headline")))
                            (list :tag "File & Regexp"
                                  (const :format "" file+regexp)
                                  ,file-variants
                                  (regexp :tag "  Regexp"))
                            (list :tag "File [ & Outline path ] & Date tree"
                                  (const :format "" file+olp+datetree)
                                  ,file-variants
                                  (option (repeat :tag "Outline path" :inline t
                                                  (string :tag "Headline"))))
                            (list :tag "File & function"
                                  (const :format "" file+function)
                                  ,file-variants
                                  (sexp :tag "  Function"))
                            (list :tag "Current clocking task"
                                  (const :format "" clock))
                            (list :tag "Function"
                                  (const :format "" function)
                                  (sexp :tag "  Function")))
                    (choice :tag "Template       "
                            (string)
                            (list :tag "File"
                                  (const :format "" file)
                                  (file :tag "Template file"))
                            (list :tag "Function"
                                  (const :format "" function)
                                  (function :tag "Template function")))
                    (plist :inline t
                           ;; Give the most common options as checkboxes
                           :options (((const :format "%v " :prepend) (const t))
                                     ((const :format "%v " :immediate-finish) (const t))
                                     ((const :format "%v " :jump-to-captured) (const t))
                                     ((const :format "%v " :empty-lines) (const 1))
                                     ((const :format "%v " :empty-lines-before) (const 1))
                                     ((const :format "%v " :empty-lines-after) (const 1))
                                     ((const :format "%v " :clock-in) (const t))
                                     ((const :format "%v " :clock-keep) (const t))
                                     ((const :format "%v " :clock-resume) (const t))
                                     ((const :format "%v " :time-prompt) (const t))
                                     ((const :format "%v " :tree-type) (const week))
                                     ((const :format "%v " :unnarrowed) (const t))
                                     ((const :format "%v " :table-line-pos) (string))
                                     ((const :format "%v " :kill-buffer) (const t))))))))
  :group 'helm-gdq-schedule)

(defcustom helm-gdq-schedule-org-capture-templates-contexts nil
  "Alist of capture templates and valid contexts.

The format is the same as in `org-capture-templates-contexts'."
  :type
  ;; copied from `org-capture-templates-contexts'
  '(repeat (list :tag "Rule"
                 (string :tag "        Capture key")
                 (string :tag "Replace by template")
                 (repeat :tag "Available when"
                         (choice
                          (cons :tag "Condition"
                                (choice
                                 (const :tag "In file" in-file)
                                 (const :tag "Not in file" not-in-file)
                                 (const :tag "In buffer" in-buffer)
                                 (const :tag "Not in buffer" not-in-buffer)
                                 (const :tag "In mode" in-mode)
                                 (const :tag "Not in mode" not-in-mode))
                                (regexp))
                          (function :tag "Custom function")))))
  :group 'helm-gdq-schedule)

;;* Globals
(cl-defstruct helm-gdq-schedule-run
  game-title
  runner
  description
  commentator
  start-time
  end-time)

;; TODO: the exact schedule url must presently be updated manually
(defvar helm-gdq-schedule-request-url "https://gamesdonequick.com/schedule"
  "URL of the ongoing marathon.")

;; `helm-resume' makes this almost entirely pointless. The only reason
;; to do this is to accommodate anybody who doesn't know about it.
(defvar helm-gdq-schedule--cache nil
  "Cache of all speedruns to avoid making too many network
calls.")

(defvar helm-gdq-schedule--last-timestamp (float-time)
  "Time of last network call, if any.")

;;* Implementation
;;** Formatting displayed strings
(defun helm-gdq-schedule--time-to-list (time-string)
  "Convert a TIME-STRING into a list with properties 'second,
'minute, and 'hour."
  (cl-destructuring-bind
      (second minute hour &rest _times)
      (parse-time-string time-string)
    (list 'second second 'minute minute 'hour hour)))

(defun helm-gdq-schedule--display-candidate (cand)
  "Display CAND according to `helm-gdq-schedule-run-format-string'."
  (format-spec helm-gdq-schedule-run-format-string
               (format-spec-make ?g (helm-gdq-schedule-run-game-title cand)
                                 ?r (helm-gdq-schedule-run-runner cand)
                                 ?d (helm-gdq-schedule-run-description cand)
                                 ?s (ts-format helm-gdq-schedule-start-timestamp-format-string
                                               (helm-gdq-schedule-run-start-time cand))
                                 ?e (ts-format helm-gdq-schedule-end-timestamp-format-string
                                               (helm-gdq-schedule-run-end-time cand))
                                 ?c (helm-gdq-schedule-run-commentator cand))))

;;** Rest
(defun helm-gdq-schedule--compute-candidates ()
  "Compute candidates for `helm'."
  (unless (and helm-gdq-schedule--cache
               (< (- (float-time) helm-gdq-schedule--last-timestamp)
                  (* 60 helm-gdq-schedule-cache-time)))
    (setq helm-gdq-schedule--cache (helm-net--url-retrieve-sync
                                    helm-gdq-schedule-request-url
                                    #'helm-gdq-schedule--parser))
    (setq helm-gdq-schedule--last-timestamp (float-time)))
  helm-gdq-schedule--cache)

;; XXX: is there a real api that returns something other than html?
(defun helm-gdq-schedule--parser ()
  "Parse HTML into a list of candidates."
  (goto-char (point-min))
  (re-search-forward "^<table")
  (let* ((beg (match-beginning 0))
         (end (re-search-forward "^</table>"))
         (table-node (libxml-parse-html-region beg end))
         (tds (cddddr (dom-by-tag table-node 'td))))
    (helm-gdq-schedule--read-speedruns tds)))

(defun helm-gdq-schedule--read-speedruns (node)
  "Generate a list of `helm-gdq-schedule-run' structs from a NODE."
  (let ((curr (mapcar #'dom-text node))
        accum)
    (while curr
      (cl-destructuring-bind (start-time
                              title
                              runner
                              setup-duration
                              gameplay-duration
                              run-description
                              commentator
                              &rest rem)
          curr
        (let* ((start-time-struct (ts-parse (replace-regexp-in-string
                                             "[a-zA-Z]" " "
                                             start-time)))
               (setup-duration-list (helm-gdq-schedule--time-to-list setup-duration))
               (gameplay-duration-list (helm-gdq-schedule--time-to-list gameplay-duration))
               (end-time-struct (apply #'ts-adjust
                                       (append setup-duration-list
                                               gameplay-duration-list
                                               (list start-time-struct))))
               (run-struct (make-helm-gdq-schedule-run :game-title title
                                                       :runner runner
                                                       :description run-description
                                                       :start-time start-time-struct
                                                       :end-time end-time-struct
                                                       :commentator commentator))
               (run-display (helm-gdq-schedule--display-candidate run-struct)))
          (push `(,run-display . ,run-struct) accum))
        (setq curr rem)))
    (nreverse accum)))

(defvar org-capture-templates)
(defvar org-capture-templates-contexts)

(defun helm-gdq-schedule-org-capture (cand)
  "Capture the selected speedrun."
  (require 'org-capture)
  ;; copied from `counsel-projectile' with appropriate changes
  (let* ((game-title (helm-gdq-schedule-run-game-title cand))
         (description (helm-gdq-schedule-run-description cand))
         (start-time (concat "<" (ts-format (helm-gdq-schedule-run-start-time cand)) ">"))
         (end-time (concat "<" (ts-format (helm-gdq-schedule-run-end-time cand)) ">"))
         (org-capture-templates-contexts
          (append helm-gdq-schedule-org-capture-templates-contexts
                  org-capture-templates-contexts))
         (org-capture-templates
          (append
           (cl-loop
            with replace-fun = (lambda (string)
                                 (replace-regexp-in-string
                                  "\\${[^}]+}"
                                  (lambda (s)
                                    (pcase s
                                      ("${game}" game-title)
                                      ("${run}" description)
                                      ("${start}" start-time)
                                      ("${end}" end-time)))
                                  string))
            for template in helm-gdq-schedule-org-capture-templates
            collect (cl-loop
                     for item in template
                     if (member (cl-position item template) '(1 4))
                     collect (funcall replace-fun item)
                     else
                     collect item))
           org-capture-templates)))
    (helm-org-capture-templates)))

;; TODO: fix persistent action
(defvar helm-source-gdq-schedule
  (helm-build-sync-source " Speedruns"
    :candidates #'helm-gdq-schedule--compute-candidates
    :action (helm-make-actions
             "Org capture" #'helm-gdq-schedule-org-capture)
    :multiline t)
  "`helm' source for `helm-gdq-schedule'.")

;;* Entry point
;;;###autoload
(defun helm-gdq-schedule ()
  "View scheduled speedruns of GDQ marathons using `helm'."
  (interactive)
  (helm :sources helm-source-gdq-schedule
        :buffer "*helm-gdq-schedule*"))

;;* Footer
(provide 'helm-gdq-schedule)
